from django.urls import path, include
from . import views
from django.contrib import admin

app_name = 'committees'

urlpatterns= [
    path('', views.committees, name='committees'),
    path('<uuid:uuid>', views.committee, name='committee'),
]