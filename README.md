# FestHub - College Event Management Website

Welcome to Spit FestHub, the centralized platform for all event-related activities at SPIT. Spit FestHub is designed to serve as the ultimate guide for students, showcasing upcoming, current, and past events organized by various committees within the college. Whether it's academic competitions, cultural festivals, or club activities, Spit FestHub aims to provide a comprehensive and user-friendly experience for both event organizers and attendees. This website is not just an event calendar but a community hub that fosters engagement and excitement around the diverse array of activities happening on our campus.

## Instructions

1. Create a virtual environment called `.env`, as follows:

```bash
python -m venv .env
```

2. Activate the virtual environment:

*Windows*
```powershell
.\.env\Scripts\Activate.ps1
```

*Ubuntu*
```bash
source .env/bin/activate
```

3. Install all the required dependencies.

```bash
pip install -r requirements.txt
```

4. Running the project.

```bash
python manage.py runserver
```

## Project Documentation

The documentation can be found here:

[Documentation](/docs/project_documentation.md)