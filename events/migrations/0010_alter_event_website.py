# Generated by Django 5.0.4 on 2024-04-25 19:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0009_alter_event_website'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='website',
            field=models.URLField(blank=True, default='#'),
        ),
    ]
