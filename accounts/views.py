from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, authenticate, logout
from django.contrib import messages
from .forms import CustomUserCreationForm, ProfileForm
from profiles.models import Profile
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth.tokens import default_token_generator
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes
from django.core.mail import send_mail
from django.conf import settings
from django.urls import reverse
from django.contrib.auth import get_user_model

def signup_view(request):
    if request.method == 'POST':
        user_form = CustomUserCreationForm(request.POST)
        if user_form.is_valid():
            username = user_form.cleaned_data.get('username')
            if CustomUserCreationForm.Meta.model.objects.filter(username=username).exists():
                messages.warning(request, 'Username already exists. Please choose a different username.')
                return redirect('accounts:signup')

            user = user_form.save(commit=False)  
            user.is_active = False  
            user.save()

            uid = urlsafe_base64_encode(force_bytes(user.pk))
            token = default_token_generator.make_token(user)

            email_subject = 'Verify your email address'
            email_body = f'Please click the following link to verify your email address:\n\n{request.build_absolute_uri(reverse("accounts:verify_email", kwargs={"uidb64": uid, "token": token}))}'
            send_mail(
                email_subject,
                email_body,
                settings.EMAIL_HOST_USER,
                [user.email],
                fail_silently=False,
            )

            messages.success(request, 'Account created successfully. Please check your email for verification instructions.')
            return redirect('accounts:login')
        else:
            for field, errors in user_form.errors.items():
                for error in errors:
                    messages.error(request, f"{field}: {error}")
    else:
        user_form = CustomUserCreationForm()

    return render(request, 'accounts/signup.html', {'user_form': user_form})



from django.contrib.auth import login
from django.contrib.auth.tokens import default_token_generator
from django.utils.http import urlsafe_base64_decode
from django.shortcuts import redirect

def verify_email(request, uidb64, token):
    User = get_user_model()
    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and default_token_generator.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)  
        messages.success(request, 'Your email has been verified. You can now access the dashboard.')
        return redirect('dashboard')  
    else:
        messages.error(request, 'Invalid verification link.')
        return redirect('accounts:login')

def completeProfile_view(request, user_id):
    user = get_object_or_404(User, pk=user_id)
    if request.method == 'POST':
        profile_form = ProfileForm(request.POST, request.FILES)
        if profile_form.is_valid():
            if not Profile.objects.filter(user=user).exists():
                profile = profile_form.save(commit=False)
                profile.user = user
            else:
                profile = Profile.objects.get(user=user)

            profile.contact_number = profile_form.cleaned_data['contact_number']
            profile.address = profile_form.cleaned_data['address']
            profile.birth_date = profile_form.cleaned_data['birth_date']
            profile.committees.set(profile_form.cleaned_data['committees'])

            # Set optional fields
            profile.resume = request.FILES.get('resume')
            profile.image = request.FILES.get('image')
            profile.department_year = profile_form.cleaned_data['department_year']
            profile.blood_group = profile_form.cleaned_data['blood_group']

            # Check if all required fields are filled except committees
            required_fields_filled = all([
                profile.contact_number,
                profile.address,
                profile.birth_date,
            ])

            if required_fields_filled:
                profile.is_profile_complete = True
                profile.save()
                return redirect('dashboard')
            else:
                profile.is_profile_complete = False
                profile.save()
                messages.warning(request, 'Please fill all required fields to access all features.')
                return redirect('accounts:completeProfile', user_id=user_id)
        else:
            for field, errors in profile_form.errors.items():
                for error in errors:
                    messages.error(request, f"{field}: {error}")
    else:
        profile_form = ProfileForm()
    return render(request, 'accounts/completeProfile.html', {'user': user, 'profile_form': profile_form})

def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(data = request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            if 'next' in request.POST:
                return redirect(request.POST.get('next'))
            return redirect('dashboard')

    else:
        form = AuthenticationForm()
    return render(request, 'accounts/login.html', {'form':form})

def logout_view(request):
    if request.method == 'POST':
        logout(request)
        return redirect('home')
